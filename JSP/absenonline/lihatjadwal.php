<?php 
  error_reporting(E_ALL && E_DEPRECATED ^ E_NOTICE ); 
  include "otentikasi.php";
  session_start();
  $username=$_SESSION["session_username"];
  $password=$_SESSION["session_password"];
  $admin_nama=$_SESSION["session_nama"];
  
  if (! empty($username)){
    $session_username=$username;
  }
  
  if (! empty($password)){
    $session_password=$password;
  }
  
  if(! otentikasi($session_username,$session_password)){
    $pesan="Anda belum login!";
    $address="http://$SERVER_NAME/absenonline/";
    header("Location: $address?pesan=$pesan");
    exit();
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Absensi Online Admin Panel</title>
        <link href="css/bootstrap-glyphicons.css" rel="stylesheet"/>               
        <link href="css/bootstrap.css" rel="stylesheet" /> 
        <link href="css/bootstrap.min.css" rel="stylesheet" />        
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>        
        <script src="js/jquery.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>

        <script>
          $( function() {
            $( "#tanggal" ).datepicker();
          } );
        </script>
    </head>

    <body>   
        <div class="container">
        <nav class="navbar navbar-default">
        <!-- menu -->
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Beranda</a>
          </div>
          <ul class="nav navbar-nav">            
            <li><a href="lihatkaryawan.php">Data Karyawan</a></li>
            <li><a href="lihat_perusahaan.php">Data Perusahaan</a></li>
            <li><a href="lihatabsensi.php">Data Absensi</a></li> 
            <li class="active"><a href="lihatjadwal.php">Data Jadwal</a></li>            
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li><a href=""><font color="blue"><?php echo $_SESSION['session_nama']; ?></font></a></li>      
		      <li><a href="logout.php">Logout</a></li>
		  </ul>
        </div>        
        <!-- end menu -->        
      </nav>	
      <a class="btn btn-primary" href="input_jadwal.php">Tambah</a> 
      <?php 
              $query="SELECT * FROM tbl_jadwal";
              $hasil=mysql_query($query) or die();     
              if(mysql_num_rows($hasil)<=0){
                  echo '<h4>Data Jadwal Masih Kosong</h4>';                
              }else{       
      ?>

      <table  class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jadwal Masuk</th>
                    <th>Jadwal Keluar</th>                    
                    <th>Keterangan</th>
                </tr>                                
            </thead>            
            <tbody>
            <?php               
                $no=1;
                while($row=mysql_fetch_array($hasil)){
                  ?>
                    <tr>       
                      <td><?php echo $no;?></td>            
                      <td><?php echo $row[1]; ?></td>
                      <td><?php echo $row[2]; ?></td>                      
                      <td>
                        <a class="btn btn-danger" href="hapus_jadwal.php?jadwal_id=<?php echo $row[0];?>">Hapus</a> 
                        <a class="btn btn-success" href="ubah_jadwal.php?jadwal_id=<?php echo $row[0];?>">Ubah</a>
                      </td>
                    </tr>
                  <?php                

                  $no++;
              }
            ?>
            </tbody>
        </table>	
        <?php } ?>
      </div>
    </body>
</html>