<?php 
  error_reporting(E_ALL && E_DEPRECATED ^ E_NOTICE ); 
  include "otentikasi.php";
  session_start();
  $username=$_SESSION["session_username"];  
  
  if (! empty($username)){
    $address="http://$SERVER_NAME/absenonline/home.php";
    header("Location: $address");
    exit();
  }   
?>
<!DOCTYPE html>
<html>
    <head>
     <meta charset="UTF-8" />
    <title>Login Panel Absensi Online Web Service</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/login.css"/>
    <link rel="stylesheet" href="css/magic.css"/>
</head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
<body >
 
   <!-- PAGE CONTENT --> 
    <div class="container">    
    <div class="tab-content">
        <div id="login" class="tab-pane active">
            <form action="create_session.php" class="form-signin" method="post">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">
                    Form Login
                </p>
                <input type="text" placeholder="Username" name="username" class="form-control" required />
                <input type="password" placeholder="Password" name="password" class="form-control" required />
                <?php 
                	if (!empty($_GET['pesan'])){ ?>
                	<font color="red"><?php echo $_GET['pesan'];?></font><br><br>
                <?php } ?>
                <button class="btn text-muted text-center btn-success" type="submit">Masuk</button>
            </form>
        </div>              
    </div>    
</div>
    <!--END PAGE CONTENT -->     	      
      <!-- PAGE LEVEL SCRIPTS -->
      <script src="js/jquery.min.js" />" ></script>
      <script src="js/bootstrap.min.js" /> "></script>
      <script src="js/login.js" />"></script>
      <!--END PAGE LEVEL SCRIPTS -->
</body>
    <!-- END BODY -->
</html>