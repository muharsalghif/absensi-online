  <?php 
  error_reporting(E_ALL && E_DEPRECATED ^ E_NOTICE ); 
  include "otentikasi.php";      
  session_start();
  $username=$_SESSION["session_username"];
  $password=$_SESSION["session_password"];
  $admin_nama=$_SESSION["session_nama"];
  
  if (! empty($username)){
    $session_username=$username;
  }
  
  if (! empty($password)){
    $session_password=$password;
  } 
  
  if(! otentikasi($session_username,$session_password)){
    $pesan="Anda belum login!";
    $address="http://$SERVER_NAME/absenonline/";
    header("Location: $address?pesan=$pesan");
    exit();
  }

  $karyawan_id=$_GET['karyawan_id'];
  $query="SELECT * FROM tbl_karyawan WHERE karyawan_id='$karyawan_id'";
  $hasil=mysql_query($query) or die();
  
  if($hasil){
  	while($row=mysql_fetch_array($hasil)){
  		$karyawan_id=$row[0];
  		$karyawan_nip=$row[1];
		$karyawan_nama=$row[2];
		$karyawan_tempat_lahir=$row[3];
		$karyawan_tgl_lahir=$row[4];
		$karyawan_jk=$row[5];
		$karyawan_alamat=trim($row[6]);
		$karyawan_email=$row[7];
		$karyawan_hp=$row[8];
		$karyawan_foto=$row[9];
		$perusahaan_id=$row[10];
		$karyawan_masuk=$row[11];
		$role_id=$row[12];
		$status_id=$row[13];
  	}
  } 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Absensi Online Admin Panel</title>
        <link href="css/bootstrap-glyphicons.css" rel="stylesheet"/>               
        <link href="css/bootstrap.css" rel="stylesheet" /> 
        <link href="css/bootstrap.min.css" rel="stylesheet" />        
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>        
        <script src="js/jquery.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>

        <script>
          $( function() {
            $( "#tanggal" ).datepicker();
          } );
        </script>
    </head>

    <body>   
        <div class="container">
        <nav class="navbar navbar-default">
        <!-- menu -->
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Beranda</a>
          </div>
          <ul class="nav navbar-nav">            
            <li  class="active"><a href="lihatkaryawan.php">Data Karyawan</a></li>
            <li><a href="lihat_perusahaan.php">Data Perusahaan</a></li>
            <li><a href="lihatabsensi.php">Data Absensi</a></li> 
            <li><a href="lihatjadwal.php">Data Jadwal</a></li>            
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li><a href=""><font color="blue"><?php echo $_SESSION['session_nama']; ?></font></a></li>      
		      <li><a href="logout.php">Logout</a></li>
		  </ul>
        </div>        
        <!-- end menu -->
      </nav>	
        <div align="center">
          <h3>Form Tambah Data Karyawan</h3><hr>
          <form class="form-horizontal" action="simpan_karyawan.php" method="post" id="block-validate">
                <div class="form-group">
                    <label class="control-label col-lg-4">NIP</label>
                    <div class="col-lg-8">
                        <input type="text" required="" name="karyawan_nip" class="form-control" placeholder="NIP Karyawan" value="<?php echo $karyawan_nip;?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Nama karyawan</label>

                    <div class="col-lg-8">
                        <input type="text" id="text2" placeholder="Nama Karyawan" name="karyawan_nama" class="form-control" required="" value="<?php echo $karyawan_nama;?>"/>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Tempat Lahir</label>

                    <div class="col-lg-8">
                        <input type="text" id="text2" placeholder="Tempat Lahir" name="karyawan_tempat_lahir" class="form-control" required="" value="<?php echo $karyawan_tempat_lahir;?>"/>
                    </div>
                </div>
                 
                <div class="form-group">
                        <label class="control-label col-lg-4" >Tanggal Lahir</label>
                        <div class="col-lg-3">
                            <div class="input-group input-append date" id="dp3" data-date="12-02-2012"
                                 data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" name="karyawan_tgl_lahir" value="01-01-1980" r value="<?php echo $karyawan_tgl_lahir;?>"/>
                                <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </div>
                </div> 
                
                    <div class="form-group">
                        <label class="control-label col-lg-4">Jenis Kelamin</label>
                        <div class="col-lg-3">                            
                                <label>
                                	<?php if ($karyawan_jk=='L'){?>
                                    <input type="radio" name="karyawan_jk" value="L" checked="checked" /> Laki-Laki 
                                    <?php }else {?>                                   
                                    <input type="radio" name="karyawan_jk" value="L" /> Laki-Laki 
                                    <?php } ?>
                                </label>      
                                &nbsp;&nbsp;                                                 
                                <label>
                                    <?php if ($karyawan_jk=='P'){?>
                                    <input type="radio" name="karyawan_jk" value="L" checked="checked" /> Perempuan
                                    <?php }else {?>                                   
                                    <input type="radio" name="karyawan_jk" value="L" /> Perempuan 
                                    <?php } ?>                                   
                                </label>                                                       
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label for="limiter" class="control-label col-lg-4">Alamat</label>

                        <div class="col-lg-8">
                            <textarea id="limiter" name="karyawan_alamat" class="form-control" required="">
                            	<?php echo $karyawan_alamat;?>
                            </textarea>
                        </div>
                    </div>
              
                    <div class="form-group">
                        <label class="control-label col-lg-4">E-mail</label>
                            <div class="col-lg-4">
                                <input type="email" id="email2" name="karyawan_email" class="form-control" required="" value="<?php echo $karyawan_email;?>"/>
                            </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="control-label col-lg-4">HP</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <input class="form-control" type="text" name="karyawan_hp" data-mask="+6299999999999" required="" value="<?php echo $karyawan_hp;?>"/>
                                <span class="input-group-addon">+6281 234 567 899</span>
                            </div>
                        </div>
                    </div>
                        
                  <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Perusahaan</label> 
                      <div class="col-lg-2">                       
                        <select name="perusahaan_id">
                              <?php 
                                $query="SELECT perusahaan_id,perusahaan_nama FROM tbl_perusahaan WHERE status_id='1'";
                                $hasil=mysql_query($query) or die();
                                if ($hasil){
                                  while($row=mysql_fetch_array($hasil)){
                                  	if($row[0]==$perusahaan_id){
                              ?>                                                 
                              <option value="<?php echo $row[0]; ?>" selected="true"><?php echo $row[1]; ?></option>                     
                              <?php }else{?>
                              <option value="<?php echo $row[0]; ?>"><?php echo $row[1]; ?></option>                                                                                              \
                              <?php }}}?>
                        </select>
                        </div>
                </div>

                <div class="form-group">
                        <label class="control-label col-lg-4" >Tanggal Masuk</label>
                        <div class="col-lg-3">
                            <div class="input-group input-append date" id="dp3" data-date="12-02-2012"
                                 data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" name="karyawan_masuk" value="01-01-1980" readonly="" />
                                <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                             </div>
                        </div>
                </div>

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Status</label> 
                      <div class="col-lg-2">                       
                        <select name="status_id">
                              <?php 
                                $query="SELECT status_id,status_nama FROM tbl_status";
                                $hasil=mysql_query($query) or die();
                                if ($hasil){
                                  while($row=mysql_fetch_array($hasil)){
                                  	if($row[0]==$status_id){
                              ?>                                                 
                              <option value="<?php echo $row[0]; ?>" selected="true"><?php echo $row[1]; ?></option>                     
                              <?php }else{?>
                             <option value="<?php echo $row[0]; ?>"><?php echo $row[1]; ?></option>                                                                                             
                              <?php }}}?>
                        </select>
                        </div>
                </div>

                <input type="hidden" name="asal" value="2">
                <input type="hidden" name="karyawan_id" value="<?php echo $karyawan_id;?>">
                <div class="form-group">
                    <label for="tags" class="control-label col-lg-4"></label>
                    <div class="col-lg-1">
                        <button type="submit" name="submit" id="submit" value="Simpan" class="btn btn-success" />Simpan
                    </div>
                </div>

            </form>

        </div>
      </div>
    </body>
</html>