<?php 
  error_reporting(E_ALL && E_DEPRECATED ^ E_NOTICE ); 
  include "otentikasi.php";
  session_start();
  $username=$_SESSION["session_username"];
  $password=$_SESSION["session_password"];
  $admin_nama=$_SESSION["session_nama"];
  
  if (! empty($username)){
    $session_username=$username;
  }
  
  if (! empty($password)){
    $session_password=$password;
  }
  
  if(! otentikasi($session_username,$session_password)){
    $pesan="Anda belum login!";
    $address="http://$SERVER_NAME/absenonline/";
    header("Location: $address?pesan=$pesan");
    exit();
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Absensi Online Admin Panel</title>
        <link href="css/bootstrap-glyphicons.css" rel="stylesheet"/>               
        <link href="css/bootstrap.css" rel="stylesheet" /> 
        <link href="css/bootstrap.min.css" rel="stylesheet" />        
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>        
        <script src="js/jquery.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>

        <script>
          $( function() {
            $( "#tanggal" ).datepicker();
          } );
        </script>
    </head>

    <body>   
        <div class="container">
        <nav class="navbar navbar-default">
        <!-- menu -->
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Beranda</a>
          </div>
          <ul class="nav navbar-nav">            
            <li><a href="lihatkaryawan.php">Data Karyawan</a></li>
            <li><a href="lihatabsensi.php">Data Absensi</a></li> 
            <li class="active"><a href="lihat_jadwal.php">Data Jadwal</a></li>            
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li><a href=""><font color="blue"><?php echo $_SESSION['session_nama']; ?></font></a></li>      
		      <li><a href="logout.php">Logout</a></li>
		  </ul>
        </div>        
        <!-- end menu -->        
      </nav>	
          
      </div>
    </body>
</html>