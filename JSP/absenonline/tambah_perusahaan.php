 <?php 
  error_reporting(E_ALL && E_DEPRECATED ^ E_NOTICE ); 
  include "otentikasi.php";      
  session_start();
  $username=$_SESSION["session_username"];
  $password=$_SESSION["session_password"];
  $admin_nama=$_SESSION["session_nama"];
  
  if (! empty($username)){
    $session_username=$username;
  }
  
  if (! empty($password)){
    $session_password=$password;
  }
  
  if(! otentikasi($session_username,$session_password)){
    $pesan="Anda belum login!";
    $address="http://$SERVER_NAME/absenonline/";
    header("Location: $address?pesan=$pesan");
    exit();
  }
   
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Absensi Online Admin Panel</title>
        <link href="css/bootstrap-glyphicons.css" rel="stylesheet"/>               
        <link href="css/bootstrap.css" rel="stylesheet" /> 
        <link href="css/bootstrap.min.css" rel="stylesheet" />        
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap.min.js"></script>        
        <script src="js/jquery.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>

        <script>
          $( function() {
            $( "#tanggal" ).datepicker();
          } );
        </script>
    </head>

    <body>   
        <div class="container">
        <nav class="navbar navbar-default">
        <!-- menu -->
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Beranda</a>
          </div>
          <ul class="nav navbar-nav">            
            <li><a href="lihatkaryawan.php">Data Karyawan</a></li>
            <li class="active"><a href="lihat_perusahaan.php">Data Perusahaan</a></li>
            <li><a href="lihatabsensi.php">Data Absensi</a></li> 
            <li><a href="lihatjadwal.php">Data Jadwal</a></li>            
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li><a href=""><font color="blue"><?php echo $_SESSION['session_nama']; ?></font></a></li>      
		      <li><a href="logout.php">Logout</a></li>
		  </ul>
        </div>        
        <!-- end menu -->
      </nav>	
        <div align="center">
          <h3>Form Tambah Data Perusahaan</h3><hr>
          <form class="form-horizontal" action="simpan_perusahaan.php" method="post" id="block-validate">
                <div class="form-group">
                    <label class="control-label col-lg-4">Kode</label>
                    <div class="col-lg-8">
                        <input type="text" required="" name="perusahaan_kode" class="form-control" placeholder="Kode Perusahaan" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Nama Perusahaan</label>

                    <div class="col-lg-8">
                        <input type="text" id="text2" placeholder="Nama Perusahaan" name="perusahaan_nama" class="form-control" required=""/>
                    </div>
                </div>
                                   
                                                
                
                <div class="form-group">
                        <label for="limiter" class="control-label col-lg-4">Alamat</label>

                        <div class="col-lg-8">
                            <textarea id="limiter" placeholder="Alamat" name="perusahaan_alamat" class="form-control" required=""></textarea>
                        </div>
                </div>              			
                            

				<div class="form-group">
                    <label for="text2" class="control-label col-lg-4">Jadwal</label> 
                      <div class="col-lg-2">                       
                        <select name="jadwal_id">
                              <?php 
                                $query="SELECT *  FROM tbl_jadwal";
                                $hasil=mysql_query($query) or die();
                                if ($hasil){
                                  while($row=mysql_fetch_array($hasil)){
                              ?>                                                 
                              <option value="<?php echo $row[0]; ?>"><?php echo $row[1].' - '.$row[2]; ?></option>                                                                                              \
                              <?php }}?>
                        </select>
                        </div>
                </div> 


                <div class="form-group">
                        <label class="control-label col-lg-4">Latitude</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <input class="form-control" type="text" name="perusahaan_lat" data-mask="-1234567" required="" placeholder="Latitude" />
                                <span class="input-group-addon">1234567</span>
                            </div>
                        </div>
                 </div>

                 <div class="form-group">
                        <label class="control-label col-lg-4">Longitude</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <input class="form-control" type="text" name="perusahaan_long" data-mask="1234567" required="" placeholder="Longitude" />
                                <span class="input-group-addon">-1234567</span>
                            </div>
                        </div>
                 </div>
 
                <input type="hidden" name="asal" value="1">       
                <div class="form-group">
                    <label for="tags" class="control-label col-lg-4"></label>
                    <div class="col-lg-1">
                        <button type="submit" name="submit" id="submit" class="btn btn-success" />Simpan
                    </div>
                </div>
            </form>

        </div>
      </div>
    </body>
</html> 