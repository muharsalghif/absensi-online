-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: absensi_db
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_absensi`
--

DROP TABLE IF EXISTS `tbl_absensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_absensi` (
  `absensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `absensi_kode` varchar(10) NOT NULL,
  `perusahaan_id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `absensi_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `absensi_keluar` timestamp NULL DEFAULT NULL,
  `absensi_fotomasuk` varchar(50) NOT NULL,
  `absensi_fotokeluar` varchar(50) NOT NULL,
  `absensi_masuk_lat` double NOT NULL,
  `absensi_masuk_long` double NOT NULL,
  `absensi_keluar_lat` double NOT NULL,
  `absensi_keluar_long` double NOT NULL,
  `absensi_status` enum('1','0') NOT NULL,
  PRIMARY KEY (`absensi_id`),
  KEY `perusahaan_id` (`perusahaan_id`),
  KEY `karyawan_id` (`karyawan_id`),
  CONSTRAINT `tbl_absensi_ibfk_1` FOREIGN KEY (`perusahaan_id`) REFERENCES `tbl_perusahaan` (`perusahaan_id`),
  CONSTRAINT `tbl_absensi_ibfk_2` FOREIGN KEY (`karyawan_id`) REFERENCES `tbl_karyawan` (`karyawan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_absensi`
--

LOCK TABLES `tbl_absensi` WRITE;
/*!40000 ALTER TABLE `tbl_absensi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_absensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_akun`
--

DROP TABLE IF EXISTS `tbl_akun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_username` varchar(32) NOT NULL,
  `akun_password` varchar(32) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  PRIMARY KEY (`akun_id`),
  KEY `karyawan_id` (`karyawan_id`),
  CONSTRAINT `tbl_akun_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `tbl_karyawan` (`karyawan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_akun`
--

LOCK TABLES `tbl_akun` WRITE;
/*!40000 ALTER TABLE `tbl_akun` DISABLE KEYS */;
INSERT INTO `tbl_akun` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3',1),(2,'user','21232f297a57a5a743894a0e4a801fc3',2);
/*!40000 ALTER TABLE `tbl_akun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_jadwal`
--

DROP TABLE IF EXISTS `tbl_jadwal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_masuk` time NOT NULL,
  `jadwal_keluar` time NOT NULL,
  PRIMARY KEY (`jadwal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_jadwal`
--

LOCK TABLES `tbl_jadwal` WRITE;
/*!40000 ALTER TABLE `tbl_jadwal` DISABLE KEYS */;
INSERT INTO `tbl_jadwal` VALUES (1,'08:00:00','17:00:00');
/*!40000 ALTER TABLE `tbl_jadwal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_karyawan`
--

DROP TABLE IF EXISTS `tbl_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_karyawan` (
  `karyawan_id` int(11) NOT NULL AUTO_INCREMENT,
  `karyawan_nip` varchar(10) NOT NULL,
  `karyawan_nama` varchar(50) NOT NULL,
  `karyawan_jk` enum('L','P') NOT NULL,
  `karyawan_alamat` varchar(100) NOT NULL,
  `karyawan_foto` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`karyawan_id`),
  KEY `role_id` (`role_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `tbl_karyawan_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`role_id`),
  CONSTRAINT `tbl_karyawan_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `tbl_status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_karyawan`
--

LOCK TABLES `tbl_karyawan` WRITE;
/*!40000 ALTER TABLE `tbl_karyawan` DISABLE KEYS */;
INSERT INTO `tbl_karyawan` VALUES (1,'A000000001','Administrator','L','Jl. Pahlawan No. 56','default.jpg',1,1),(2,'A000000002','Andi Wijaya','L','Jl. K.H Ahmad Dahlan No. 76','default.jpg',2,2),(3,'A000000003','Rini Sumarni','P','Jl. A. Yani No. 82','default.jpg',1,2);
/*!40000 ALTER TABLE `tbl_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_perusahaan`
--

DROP TABLE IF EXISTS `tbl_perusahaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_perusahaan` (
  `perusahaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_nama` varchar(50) NOT NULL,
  `perusahaan_alamat` varchar(100) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `perusahaan_lat` double NOT NULL,
  `perusahaan_long` double NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`perusahaan_id`),
  KEY `jadwal_id` (`jadwal_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `tbl_perusahaan_ibfk_1` FOREIGN KEY (`jadwal_id`) REFERENCES `tbl_jadwal` (`jadwal_id`),
  CONSTRAINT `tbl_perusahaan_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `tbl_status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_perusahaan`
--

LOCK TABLES `tbl_perusahaan` WRITE;
/*!40000 ALTER TABLE `tbl_perusahaan` DISABLE KEYS */;
INSERT INTO `tbl_perusahaan` VALUES (1,'PT Bank Negara Indonesia','alamat',1,-0,0,1);
/*!40000 ALTER TABLE `tbl_perusahaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_nama` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_role`
--

LOCK TABLES `tbl_role` WRITE;
/*!40000 ALTER TABLE `tbl_role` DISABLE KEYS */;
INSERT INTO `tbl_role` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `tbl_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status`
--

DROP TABLE IF EXISTS `tbl_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_nama` varchar(20) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status`
--

LOCK TABLES `tbl_status` WRITE;
/*!40000 ALTER TABLE `tbl_status` DISABLE KEYS */;
INSERT INTO `tbl_status` VALUES (1,'Aktiv'),(2,'Non Aktiv');
/*!40000 ALTER TABLE `tbl_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-15 16:32:57
