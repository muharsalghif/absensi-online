package com.absenonline.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.absenonline.model.Karyawan;
import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static final String db_name="absen_online_db.db";
    private static final String table_karyawan="tbl_karyawan";
    private static final String kolom_karyawan_id="karyawan_id";
    private static final String kolom_karyawan_nip="karyawan_nip";
    private static final String kolom_karyawan_nama="karyawan_nama";
    private static final String kolom_karyawan_jk="karyawan_jk";
    private static final String kolom_karyawan_alamat="karyawan_alamat";
    private static final String kolom_karyawan_foto="karyawan_foto";
    private static final String kolom_role_id="role_id";
    private static final String kolom_status_id="status_id";

    public DBHelper(Context context) {
        super(context, db_name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+table_karyawan+"("+"" +
                kolom_karyawan_id+" INTEGER NOT NULL PRIMARY KEY,"+
                kolom_karyawan_nip+" VARCHAR(10) NOT NULL,"+
                kolom_karyawan_nama+" VARCHAR(50) NOT NULL,"+
                kolom_karyawan_jk+" VARCHAR(1) NOT NULL,"+
                kolom_karyawan_alamat+" VARCHAR(100) NOT NULL,"+
                kolom_karyawan_foto+" VARCHAR(100) NOT NULL,"+
                kolom_role_id+" INTEGER NOT NULL,"+
                kolom_status_id+" INTEGER NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+table_karyawan);
        onCreate(db);
    }

    public boolean insertKaryawan(Karyawan karyawan){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(kolom_karyawan_id, String.valueOf(karyawan.getRole_id()));
            contentValues.put(kolom_karyawan_nip, karyawan.getKaryawan_nip());
            contentValues.put(kolom_karyawan_nama,karyawan.getKaryawan_nama());
            contentValues.put(kolom_karyawan_jk, karyawan.getKaryawan_jk());
            contentValues.put(kolom_karyawan_alamat, karyawan.getKaryawan_alamat());
            contentValues.put(kolom_karyawan_foto, karyawan.getKaryawan_foto());
            contentValues.put(kolom_role_id, String.valueOf(karyawan.getRole_id()));
            contentValues.put(kolom_status_id, String.valueOf(karyawan.getStatus_id()));

            db.insert(table_karyawan, null, contentValues);
            db.close();

            return true;
        }catch (Exception e){
            Log.e("INSERT_KARYAWAN"," Gagal menambah data Karyawan");
            return false;
        }
    }

    public Karyawan getKaryawanById(int karyawan_id){
        Karyawan karyawan=null;

        String query="SELECT * FROM "+table_karyawan+" WHERE "+kolom_karyawan_id+"="+karyawan_id;

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                karyawan=new Karyawan();
                karyawan.setKaryawan_id(Integer.valueOf(cursor.getInt(0)));
                karyawan.setKaryawan_nip(cursor.getString(1));
                karyawan.setKaryawan_nama(cursor.getString(2));
                karyawan.setKaryawan_jk(cursor.getString(3));
                karyawan.setKaryawan_alamat(cursor.getString(4));
                karyawan.setKaryawan_foto(cursor.getString(5));
                karyawan.setRole_id(Integer.valueOf(cursor.getInt(6)));
                karyawan.setStatus_id(Integer.valueOf(cursor.getInt(7)));
            } while(cursor.moveToNext());
        }
        return karyawan;
    }

    public List<Karyawan> getAllKaryawan(){
        List<Karyawan> listKaryawan=new ArrayList<>();

        String query="SELECT * FROM "+table_karyawan;

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Karyawan karyawan=new Karyawan();
                karyawan.setKaryawan_id(Integer.valueOf(cursor.getInt(0)));
                karyawan.setKaryawan_nip(cursor.getString(1));
                karyawan.setKaryawan_nama(cursor.getString(2));
                karyawan.setKaryawan_jk(cursor.getString(3));
                karyawan.setKaryawan_alamat(cursor.getString(4));
                karyawan.setKaryawan_foto(cursor.getString(5));
                karyawan.setRole_id(Integer.valueOf(cursor.getInt(6)));
                karyawan.setStatus_id(Integer.valueOf(cursor.getInt(7)));
                listKaryawan.add(karyawan);
            }while(cursor.moveToNext());
        }
        return listKaryawan;
    }



    public boolean deleteKaryawan(Karyawan karyawan){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(table_karyawan,"karyawan_id=?",new String[]{Integer.toString(karyawan.getKaryawan_id())});
            db.close();
            return true;
        }catch(Exception e) {
            Log.e("DELETE KARYAWAN", "Gagal menghapus data Karyawan");
            return false;
        }
    }

    public boolean logout(){
        try{
            SQLiteDatabase db = this.getWritableDatabase();

            db.delete(table_karyawan,null,null);
            db.close();
            return true;
        }catch(Exception e) {
            Log.e("LOGOUT_PROCESS", "Gagal Logout ");
            return false;
        }
    }
}
