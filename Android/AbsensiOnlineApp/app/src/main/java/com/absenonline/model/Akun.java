package com.absenonline.model;
public class Akun {
    private int akun_id;
    private String akun_username;
    private String akun_password;
    private Karyawan karyawan_id;

    public int getAkun_id() {
        return akun_id;
    }

    public void setAkun_id(int akun_id) {
        this.akun_id = akun_id;
    }

    public String getAkun_username() {
        return akun_username;
    }

    public void setAkun_username(String akun_username) {
        this.akun_username = akun_username;
    }

    public String getAkun_password() {
        return akun_password;
    }

    public void setAkun_password(String akun_password) {
        this.akun_password = akun_password;
    }

    public Karyawan getKaryawan_id() {
        return karyawan_id;
    }

    public void setKaryawan_id(Karyawan karyawan_id) {
        this.karyawan_id = karyawan_id;
    }
}
