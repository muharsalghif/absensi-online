package com.absenonline.app;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DaftarAbsenActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spinner;
    ArrayAdapter<CharSequence> adapter;
    EditText et_tgl;
    public SimpleDateFormat dateFormatter;
    TextView tvPenempatan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_absen);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        spinner= (Spinner) findViewById(R.id.spinner);
        et_tgl = (EditText) findViewById(R.id.et_tgl);
        et_tgl.setOnClickListener(this);

        tvPenempatan = (TextView) findViewById(R.id.tvPenempatan);

        adapter= ArrayAdapter.createFromResource(this,R.array.nama_penempatan,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position)+" selected ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    @Override
    public void onClick(View v) {
        if(v==et_tgl){
            GregorianCalendar cal=new GregorianCalendar();

            DatePickerDialog dpd=new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    //et_tgl_lahir.setText(dayOfMonth+"/"+(monthOfYear + 1)+"/"+year);
                    String date=dateFormatter.format(newDate.getTime());
                    et_tgl.setText(date);
                }
            },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            dpd.show();
        }
    }
}

