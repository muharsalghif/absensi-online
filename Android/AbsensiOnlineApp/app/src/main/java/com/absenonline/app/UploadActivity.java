package com.absenonline.app;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadActivity extends AppCompatActivity implements View.OnClickListener {

    static int request_image = 1;
    static int media_image = 100;
    static String directory = "Hello_Camera";
    Uri fileUri=null;
    Button image, upload;
    ImageView iv;
    private LatLng perusahaan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        iv = (ImageView) findViewById(R.id.imageView);
        image = (Button) findViewById(R.id.btimage);
        upload = (Button) findViewById(R.id.btUpload);

        image.setOnClickListener(this);
        upload.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == image) {
//            cekLokasi();
            CaptureImage();
        }else if (view == upload){
            if (this.fileUri!=null){
                Toast.makeText(this, "Anda Berhasil Upload", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(this, StatusUploadActivity.class);
//                startActivity(intent);
            }else{
                Toast.makeText(this, "Anda Belum Foto", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void CaptureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        this.fileUri = getMediaFileUri(media_image);
        intent.putExtra("output", this.fileUri);
        startActivityForResult(intent, request_image);
    }

    public Uri getMediaFileUri(int type) {
        return Uri.fromFile(getMediaFile(type));
    }

    private File getMediaFile(int type) {
        File mediaStorage = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), directory);
        if (mediaStorage.exists() || mediaStorage.mkdirs()) {
            String timestamp = new SimpleDateFormat("yyyMMdd_HHmmss").format(new Date());
            if (type == media_image) {
                return new File(mediaStorage.getPath() + File.separator + "IMG_" + timestamp + ".jpg");

            }
            return null;
        }
        return null;
    }

    public void onActivityResult(int requestCode, int result_code,Intent data) {
        if (requestCode == request_image) {
            if (result_code == RESULT_OK) {
                iv.setImageBitmap(BitmapFactory.decodeFile(this.fileUri.getPath()));
            }
        }
    }

    private void cekLokasi() {

//        Toast.makeText(this, "Lat : " + myLatitude + ", Long : " + myLongitude, Toast.LENGTH_SHORT).show();
////
//        function distance(lat1, lon1, lat2, lon2, unit) {
//            var radlat1 = Math.PI * lat1/180
//            var radlat2 = Math.PI * lat2/180
//            var theta = lon1-lon2
//            var radtheta = Math.PI * theta/180
//            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
//            dist = Math.acos(dist)
//            dist = dist * 180/Math.PI
//            dist = dist * 60 * 1.1515
//            if (unit=="K") { dist = dist * 1.609344 }
//            if (unit=="N") { dist = dist * 0.8684 }
//            return dist
//        }
    }

}
