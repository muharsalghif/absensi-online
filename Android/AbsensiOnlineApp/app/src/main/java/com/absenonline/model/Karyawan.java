package com.absenonline.model;

public class Karyawan {
    private int karyawan_id;
    private String karyawan_nip;
    private String karyawan_nama;
    private String karyawan_jk;
    private String karyawan_alamat;
    private String karyawan_foto;
    private int role_id;
    private int status_id;

    public int getKaryawan_id() {
        return karyawan_id;
    }

    public void setKaryawan_id(int karyawan_id) {
        this.karyawan_id = karyawan_id;
    }

    public String getKaryawan_nip() {
        return karyawan_nip;
    }

    public void setKaryawan_nip(String karyawan_nip) {
        this.karyawan_nip = karyawan_nip;
    }

    public String getKaryawan_nama() {
        return karyawan_nama;
    }

    public void setKaryawan_nama(String karyawan_nama) {
        this.karyawan_nama = karyawan_nama;
    }

    public String getKaryawan_jk() {
        return karyawan_jk;
    }

    public void setKaryawan_jk(String karyawan_jk) {
        this.karyawan_jk = karyawan_jk;
    }

    public String getKaryawan_alamat() {
        return karyawan_alamat;
    }

    public void setKaryawan_alamat(String karyawan_alamat) {
        this.karyawan_alamat = karyawan_alamat;
    }

    public String getKaryawan_foto() {
        return karyawan_foto;
    }

    public void setKaryawan_foto(String karyawan_foto) {
        this.karyawan_foto = karyawan_foto;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }
}
