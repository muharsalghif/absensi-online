//package com.absenonline.app;
//
//import android.content.Intent;
//import android.support.annotation.NonNull;
//import android.support.design.widget.NavigationView;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.absenonline.db.DBHelper;
//
////public class MenuAdminActivity extends AppCompatActivity
//     implements NavigationView.OnNavigationItemSelectedListener {
//
//        DBHelper db;
//
//        @Override
//        protected void onCreate (Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_navigation_bar);
//
//        db = new DBHelper(this);
//
//        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(tb);
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, tb, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//    }
//
//        @Override
//        public void onBackPressed () {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//        @Override
//        public boolean onCreateOptionsMenu (Menu menu){
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.navigation_bar, menu);
//        return true;
//    }
//
//        @Override
//        public boolean onOptionsItemSelected (MenuItem item){
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//
//
//        return super.onOptionsItemSelected(item);
//    }
//
////    @Override
////    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
////        int id = item.getItemId();
////
////        if (id==R.id.nav_daftarkaryawan){
////            Intent intent = new Intent(MenuAdminActivity.this, MenuKaryawanActivity.class);
////            startActivity(intent);
////        }else if (id==R.id.nav_daftarabsen) {
////            Intent intent = new Intent(MenuAdminActivity.this, ProfilActivity.class);
////            startActivity(intent);
////        }else if (id==R.id.nav_listpenempatan) {
////            Intent intent = new Intent(MenuAdminActivity.this, ProfilActivity.class);
////            startActivity(intent);
////        } else if (id==R.id.nav_logout1) {
////            if (db.logout()) {
////                Toast.makeText(this, "Logout sukses", Toast.LENGTH_SHORT).show();
////                startActivity(new Intent(MenuAdminActivity.this, LoginActivity.class));
////                finish();
////            } else {
////                Toast.makeText(this, "Logout gagal", Toast.LENGTH_SHORT).show();
////            }
////
////        }
////        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
////        drawer.closeDrawer(GravityCompat.START);
////        return true;
////    }
//}
