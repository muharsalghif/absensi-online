package com.absenonline.model;
public class Role {
    private int role_id;
    private String role_nama;

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole_nama() {
        return role_nama;
    }

    public void setRole_nama(String role_nama) {
        this.role_nama = role_nama;
    }
}
