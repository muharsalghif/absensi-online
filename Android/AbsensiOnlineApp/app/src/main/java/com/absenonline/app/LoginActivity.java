package com.absenonline.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.absenonline.db.DBHelper;
import com.absenonline.globalvar.MyGlobalVar;
import com.absenonline.model.Karyawan;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btn_login,btn_daftar;
    private EditText et_username,et_password;
    private String username=null;
    private String password=null;
    private DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db=new DBHelper(this);
        btn_login=(Button) findViewById(R.id.id_btn_login);
        btn_daftar=(Button) findViewById(R.id.id_btn_daftar);
        et_username=(EditText) findViewById(R.id.id_et_login_username);
        et_password=(EditText) findViewById(R.id.id_et_login_password);

        btn_login.setOnClickListener(this);
        btn_daftar.setOnClickListener(this);

        if(db.getAllKaryawan().size()>0){
            if(db.getAllKaryawan().get(0).getRole_id()==1){
                startActivity(new Intent(LoginActivity.this,NavigationAdminActivity.class));
            }else if(db.getAllKaryawan().get(0).getRole_id()==2){
                startActivity(new Intent(LoginActivity.this,NavigationBarActivity.class));
            }

            finish();
        }

    }

    @Override
    public void onClick(View v) {
        if(v==btn_login){
            loginProcess();
        }else if(v==btn_daftar){
            startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
        }
    }

    private void loginProcess(){
        username=et_username.getText().toString().trim();
        password=et_password.getText().toString().trim();

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        String url= MyGlobalVar.getAbsenURL()+"login_user.php";

        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String status=response.trim();
                Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                if(!status.equals("null")) {
                    Gson gson=new Gson();
                    Karyawan karyawan=gson.fromJson(status,Karyawan.class);

                    if(db.insertKaryawan(karyawan)){
                        Toast.makeText(LoginActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(LoginActivity.this, "Local Database Failed", Toast.LENGTH_SHORT).show();
                    }

                    if (karyawan.getRole_id() == 1) {
                        startActivity(new Intent(LoginActivity.this,NavigationAdminActivity.class));
                    } else if (karyawan.getRole_id() == 2) {
                        startActivity(new Intent(LoginActivity.this,NavigationBarActivity.class));
                    }
                    finish();
                }else{
                    AlertDialog.Builder ad=new AlertDialog.Builder(LoginActivity.this);
                    ad.setCancelable(false);
                    ad.setMessage("Username atau password salah !");
                    ad.setTitle("Login");

                    ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            et_username.setText("");
                            et_password.setText("");
                            et_username.requestFocus();
                        }
                    }).create().show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this,"Error karena : "+error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param=new HashMap<String, String>();
                param.put("username",username);
                param.put("password",password);
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }
}
