package com.absenonline.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.absenonline.db.DBHelper;

public class MenuKaryawanActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_profil,btn_logout, btn_absen;
    DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_karyawan);

        db = new DBHelper(this);

        btn_profil=(Button) findViewById(R.id.id_btn_menu_profil);
        btn_logout=(Button) findViewById(R.id.id_btn_menu_logout);
        btn_absen = (Button) findViewById(R.id.id_btn_menu_absen);

        btn_logout.setOnClickListener(this);
        btn_profil.setOnClickListener(this);
        btn_absen.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v==btn_logout){
            logoutProcess();
        }else if(v==btn_profil){
            startActivity(new Intent(MenuKaryawanActivity.this,ProfilActivity.class));
        }else if (v==btn_absen)
            startActivity(new Intent(MenuKaryawanActivity.this,UploadActivity.class));
    }

    private void logoutProcess(){
        if(db.logout()){
            Toast.makeText(this, "Logout sukses", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(MenuKaryawanActivity.this,LoginActivity.class));
            finish();
        }else{
            Toast.makeText(this, "Logout gagal", Toast.LENGTH_SHORT).show();
        }
    }
}
